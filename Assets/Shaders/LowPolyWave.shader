// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'


Shader "Custom/Unlit/LowPolyWave"
{
Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseMap ("Noise Map", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_Magnitude("Magnitude", Float) = 0.5
		_TimeScale("Time Scale", Range(0, 1)) = 0.5
		_ShadowStrength("Shadow Strength", Range(0,1)) = 0.5
		_Dist ("Fade Distance", Float) = 20.0
        _Fade ("Fade Intensity", Float) = 1.0
		_WaveLength("Wave Length", Float) = 5.0
		_WaveSpeed("Wave Speed", Float) = 5.0

	}
	SubShader
	{
		Tags
		{
			"RenderType"="Opaque"
			"LightMode"="ForwardBase"
		}

		Pass
		{
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			#if SHADER_API_GLES
			#pragma glsl
			#endif

			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc" // for _LightColor0
			#include "AutoLight.cginc"

			struct appdata
			{
				half4 vertex : POSITION;
				half2 uv : TEXCOORD0;
			};

			struct v2f
			{
				half4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				SHADOW_COORDS(1)
				half3 viewPos :TEXCOORD2;
				half3 worldPos :TEXCOORD3;
			};
			
			sampler2D _MainTex;
			sampler2D _NoiseMap;
			half4 _Color;
			half _Magnitude;
			half _TimeScale;
			half _Dist;
			half _Fade;
			half _ShadowStrength;
			half _WaveLength;
			half _WaveSpeed;

		
			v2f vert (appdata v)
			{
				v2f o;
				
				
				half2 uvPos = v.uv + (_Time.x * _TimeScale);
				half4 noise = tex2Dlod(_NoiseMap, half4(uvPos,0,0)) ;
				half dispAmount = (sin(_Time * - _WaveSpeed + v.uv.y * _WaveLength) - 0.5) * noise.x;
				v.vertex.y += dispAmount * _Magnitude ;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;

				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.viewPos = UnityObjectToViewPos(v.vertex);
				
               
				TRANSFER_SHADOW(o)
                return o;
			}

			
			half4  frag (v2f i) : SV_Target
			{
				half shadow = saturate (lerp (SHADOW_ATTENUATION(i), 1.0, (distance (i.worldPos.xyz, _WorldSpaceCameraPos.xyz) - _Dist) * _Fade));
				#if SHADER_API_GLES
					half3 x = dFdx(i.viewPos);
					half3 y = dFdy(i.viewPos);
				#else
					half3 x = ddx(i.viewPos);
					half3 y = ddy(i.viewPos);
				#endif

				 
				half3 normal = normalize(cross(x,y));
				half ndotl = dot(normal, _WorldSpaceLightPos0);
			
				return _Color * ndotl  * saturate(shadow + (1 -_ShadowStrength)) ;
			}
			ENDCG
		}
	}
	Fallback "Mobile/Diffuse"
}	