﻿using UnityEngine;

public class ImmunityEffect : MonoBehaviour
{
    public ParticleSystem haloParticle;
    public ParticleSystem sparklesParticle;

    public float duration = 10;
    
    public void SetDuration(float dur)
    {
        gameObject.SetActive(true);

        duration = dur;
        var haloMain = haloParticle.main;
        haloParticle.Stop(false ,ParticleSystemStopBehavior.StopEmittingAndClear);
        haloMain.startLifetime = duration;
        haloMain.duration = duration;
        haloParticle.Play();

        var sparklesMain = sparklesParticle.main;
        sparklesParticle.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
        sparklesMain.duration = duration;
        sparklesParticle.Play();
    }

}
