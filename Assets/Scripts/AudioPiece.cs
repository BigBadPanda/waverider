using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPiece : MonoBehaviour
{
    public float minDistance;
    public float maxDistance;

    public AudioClip clip;
    public bool loop;

    void Start()
    {
        if (!AudioManager.Instance.isInitilized)
            return;

        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.minDistance = minDistance;
        source.maxDistance = maxDistance;
        source.clip = clip;
        source.loop = loop;
        source.spatialBlend = 1;
        source.rolloffMode = AudioRolloffMode.Linear;

        source.Play();
    }
}
