using UnityEngine;

public class TutorialManager : MonoBehaviour {

    public bool tutorialEnabled;

    public float timeSlowDownPercent;
    public GameObject[] tutorials;
    public GameObject tutorialTile;


    int currentTutorialIdx = -1;
    GameObject[] spawnedTutorials;
    TileManager tileManager;

    private void Awake()
    {
        if (PlayerPrefs.GetInt("TUTORIAL") != 1)
        {
            InitiateTutorials();
        }
    }


    void InitiateTutorials()
    {
        PlayerPrefs.SetInt("TUTORIAL", 1);
        spawnedTutorials = new GameObject[tutorials.Length];

        tileManager = FindObjectOfType<TileManager>();
        Canvas canvas = FindObjectOfType<Canvas>();

        for (int i = 0; i < tutorials.Length; i++)
        {
            spawnedTutorials[i] = Instantiate(tutorials[i], canvas.transform).gameObject;
            spawnedTutorials[i].SetActive(false);
        }
        Tile tile = Instantiate(tutorialTile, transform).GetComponent<Tile>();
        tile.Randomize();
        tileManager.SetTutorial(tile);
    }

    public void EnterTutorialZone(int tutorialIdx)
    {
        currentTutorialIdx = tutorialIdx;
        spawnedTutorials[currentTutorialIdx].SetActive(true);

        Time.timeScale = timeSlowDownPercent;
    }

    public void ExitTutorialZone(int tutorialIdx)
    {
        spawnedTutorials[tutorialIdx].SetActive(false);
        Time.timeScale = 1;
    }
}
