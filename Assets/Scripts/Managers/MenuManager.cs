using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [Header("Menus")]
    public GameObject OptionsCanvas;
    public GameObject MainMenuCanvas;

    [Header("Toggles")]
    public Toggle SFXToggle;
    public Toggle MusicToggle;
    public Toggle GraphicsToggle;
    
    void Awake()
    {
        CheckOptions();
    }

    void CheckOptions()
    {
        MusicToggle.isOn = (PlayerPrefs.GetInt("MUSIC") == 1);
        SFXToggle.isOn = (PlayerPrefs.GetInt("SFX") == 1);
    }

    public void OnPlayClicked()
    {
        StartGame();
    }

	public void StartGame()
    {
        ContextManager.Instance.LoadContext("Game");
    }

    public void OnOptionsClicked()
    {
        OptionsCanvas.SetActive(true);
        MainMenuCanvas.SetActive(false);
    }

    public void OnMainMenuClicked()
    {
        MainMenuCanvas.SetActive(true);
        OptionsCanvas.SetActive(false);
    }

    public void OnTutorialResetClicked()
    {
        PlayerPrefs.SetInt("TUTORIAL", 0);
    }
    
    public void MusicChanged(Toggle value)
    {
        if (value.isOn)
        {
            PlayerPrefs.SetInt("MUSIC", 1);
        }
        else
        {
            PlayerPrefs.SetInt("MUSIC", 0);
        }

        ContextManager.Instance.CheckOptions();
    }

    public void SFXChanged(Toggle value)
    {
        if (value.isOn)
        {
            PlayerPrefs.SetInt("SFX", 1);
        }
        else
        {
            PlayerPrefs.SetInt("SFX", 0);
        }
        ContextManager.Instance.CheckOptions();
    }

}
