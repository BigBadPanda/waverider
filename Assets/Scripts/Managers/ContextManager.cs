using UnityEngine;
using UnityEngine.SceneManagement;

public class ContextManager : MonoBehaviour {

    Scene CurrentContext;

    public static ContextManager Instance;
   // public GameObject LoadingUI;

    private bool isBonusRequested = false;

    void Awake()
    {
        Application.targetFrameRate = 40;

        if (Instance == null)
        {
            Instance = this;
        }

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;

        if (!PlayerPrefs.HasKey("MUSIC"))
        {
            PlayerPrefs.SetInt("MUSIC", 1);
        }
        if (!PlayerPrefs.HasKey("SFX"))
        {
            PlayerPrefs.SetInt("SFX", 1);
        }

        PlayerPrefs.SetInt("AD_COUNT", Random.Range(3, 6));


        CheckOptions();
        Instance.LoadContext("Menu");
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    public void LoadContext(string ContextName)
    {
        if (CurrentContext.name != null)
        {
            SceneManager.UnloadSceneAsync(CurrentContext);
        }

        //LoadingUI.SetActive(true);

        switch (ContextName)
	    {
            case "Menu":
                SceneManager.LoadScene("MainMenuContext", LoadSceneMode.Additive);
                break;
            case "Game":
                SceneManager.LoadScene("GameContext", LoadSceneMode.Additive);
                break;
            case "Bonus":
                SceneManager.LoadScene("MainMenuContext", LoadSceneMode.Additive);
                isBonusRequested = true;
                break;
		    default:
                Debug.LogError("Context name not defined");
            break;
	    }

        Time.timeScale = 1;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (arg0.name == "MainScene")
            return;

        Debug.Log(string.Format("Scene Load Finished {0} mode: {1}", arg0.name, arg1));
        //LoadingUI.SetActive(false);
        CurrentContext = arg0;
    }

    public bool isBonusMenuRequested()
    {
        bool result = isBonusRequested;
        isBonusRequested = false;
        return result;
    }

    public void CheckOptions()
    {
        if (PlayerPrefs.GetInt("MUSIC") == 1)
        {
            GetComponent<AudioSource>().enabled = true;
        }
        else
        {
            GetComponent<AudioSource>().enabled = false;
        }
    }
}
