using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using TMPro;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    [Header("Score Fields")]
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highScoreText;

    [Header("Menu Objects")]
    public TextMeshProUGUI pauseMessageText;
    public TextMeshProUGUI highscoreBoardText;
    public TextMeshProUGUI bestscoreText;
    public GameObject menuButton;
    public GameObject playButton;
    public GameObject adMenu;
    public GameObject highScoreBoard;
    public UIElementController multiplierPlateController;
    public UIElementController windzonePlateController;
    public GameObject inputBlocker;

    [Header("Audios")]
    public AudioClip windAudio;

    private AudioSource audioSource;

    [Header("Unity ADS")]
    public float secondsForAD = 3;
    public float minScoreForAD = 100;

    float currentScore;
    float highscore;
    float multiplier = 1;
    bool inWindzone;
    bool isSecondChance;

    bool gameEnded;
    bool gamePaused;
    ShipBehaviour shipBehaviour;

    Coroutine adMenuCoroutine;

    private void Awake()
    {
        shipBehaviour = FindObjectOfType<ShipBehaviour>();
        audioSource = GetComponent<AudioSource>();

        CheckPlayerPrefs();
    }

    void CheckPlayerPrefs()
    {
        highscore = PlayerPrefs.GetInt("HIGHSCORE");
    }

    void PlayerScored(object data)
    {
        if(inWindzone)
        {
            currentScore += multiplier * 2;
        }
        else
        {
            currentScore += multiplier;
        }
        scoreText.text = string.Format("Score: {0}", currentScore);
    }

    void WindzoneChange(object data)
    {
        inWindzone = (bool) data;

        if((bool)data)
        {
            AudioManager.Instance.SetActiveWindzoneAudio(true);
            windzonePlateController.gameObject.SetActive(true);
            windzonePlateController.Enter();
        }
        else if(windzonePlateController.gameObject.activeSelf)
        {
            AudioManager.Instance.SetActiveWindzoneAudio(false);
            windzonePlateController.Exit();
        }
    }

    public void OnPauseClicked()
    {
        if (gameEnded)
            return;

        if (!gamePaused)
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    public void OnMenuClicked()
    {
        ContextManager.Instance.LoadContext("Menu");
    }

    public void OnPlayClicked()
    {
        ContextManager.Instance.LoadContext("Game");
    }

    void PauseGame()
    {
        gamePaused = true;

        pauseMessageText.text = "Game\nPaused!";
        pauseMessageText.gameObject.SetActive(true);

        menuButton.SetActive(true);

        Time.timeScale = 0;
    }

    void ResumeGame()
    {
        if (resumeInAction)
            return;

        menuButton.SetActive(false);
        StartCoroutine("resumeTimeTick");
    }

    void EndGame(object data)
    {
        gameEnded = true;
        if(currentScore >= minScoreForAD && !isSecondChance && ShowAdMenu())
        {
            adMenuCoroutine = StartCoroutine(AdMenuRoutine());
          
        }
        else
        {
            StartCoroutine(GameoverRoutine());
        }
       
        
    }

    void MultiplierChange(object multiplier_Change)
    {
        if ((int)multiplier_Change < 1)
        {
            multiplier = 1;
            multiplierPlateController.Exit();
        }
        else
        {
            multiplier = (int)multiplier_Change;
            multiplierPlateController.gameObject.SetActive(true);
            multiplierPlateController.Enter("x" + multiplier);
        }
        
    }

    bool resumeInAction;
    IEnumerator resumeTimeTick()
    {
        resumeInAction = true;

        float time = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup < time + 3)
        {
            pauseMessageText.text = string.Format("{0}!", Mathf.CeilToInt(time + 3 - Time.realtimeSinceStartup));
            yield return null;
        }

        pauseMessageText.gameObject.SetActive(false);

        Time.timeScale = 1;
        gamePaused = false;

        resumeInAction = false;
    }

    

    IEnumerator AdMenuRoutine()
    {
        shipBehaviour.TriggerAnimation("Hit");
        isSecondChance = true;
        yield return new WaitForSeconds(secondsForAD);
        adMenu.SetActive(false);
        Debug.Log("Game over calling");
        yield return GameoverRoutine();
    }

    IEnumerator GameoverRoutine()
    {
        if (PlayerPrefs.GetInt("AD_COUNT") <= 0 && ShowAdMenu())
        {
            ShowAdPlacement();
        }
        else
        {
            PlayerPrefs.SetInt("AD_COUNT", PlayerPrefs.GetInt("AD_COUNT") - 1);
        }
        shipBehaviour.TriggerAnimation("Death");

        adMenu.SetActive(false);
        menuButton.SetActive(true);
        playButton.SetActive(true);

        if(currentScore > highscore)
        {
            PlayerPrefs.SetInt("HIGHSCORE", (int)currentScore);
            highscoreBoardText.text = "High Score!";
            bestscoreText.text = currentScore.ToString();
        }
        else
        {
            bestscoreText.text = highscore.ToString();
        }

        highScoreText.text = currentScore.ToString();
        highScoreBoard.SetActive(true);

        Analytics.CustomEvent("gameOver", new Dictionary<string, object>
            {
                {"score", (int)currentScore },
                {"distance", (int)shipBehaviour.GetDistanceTraveled() }
            });

        yield return null;
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.PLAYER_SCORED, PlayerScored);
        EventManager.StartListening(Events.WINDZONE_CHANGE, WindzoneChange);
        EventManager.StartListening(Events.PLAYER_DIED, EndGame);
        EventManager.StartListening(Events.MULTIPLIER_RECEIVED, MultiplierChange);

        Time.timeScale = 1;
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PLAYER_SCORED, PlayerScored);
        EventManager.StopListening(Events.WINDZONE_CHANGE, WindzoneChange);
        EventManager.StopListening(Events.PLAYER_DIED, EndGame);
        EventManager.StopListening(Events.MULTIPLIER_RECEIVED, MultiplierChange);
    }

    private bool ShowAdMenu()
    {
        if(Advertisement.IsReady())
        {
            adMenu.SetActive(true);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ShowSecondChanceAdPlacement()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = SecondChanceAdCompleted;

        StopCoroutine(adMenuCoroutine);
        inputBlocker.SetActive(true);
        Advertisement.Show("rewardedVideo", options);

        Analytics.CustomEvent("adPop", new Dictionary<string, object>
            {
                {"score", (int)currentScore },
                {"distance", (int)shipBehaviour.GetDistanceTraveled() }
            });
    }
   
    private void SecondChanceAdCompleted(ShowResult result)
    {
        inputBlocker.SetActive(false);
        if (result == ShowResult.Finished)
        {
            Analytics.CustomEvent("adWatched", new Dictionary<string, object>
            {
                {"score", (int)currentScore },
                {"distance", (int)shipBehaviour.GetDistanceTraveled() }
            });
            adMenu.SetActive(false);
            shipBehaviour.SecondChance();
            gameEnded = false;
        }
        else
        {
            StartCoroutine(GameoverRoutine());
        }
        PlayerPrefs.SetInt("AD_COUNT", Random.Range(5, 9));
    }

    private void ShowAdPlacement()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCompleted;
        
        Advertisement.Show("video", options);
        inputBlocker.SetActive(true);
    }

    private void AdCompleted(ShowResult result)
    {
        inputBlocker.SetActive(false);
        if (result == ShowResult.Finished)
        {
            Analytics.CustomEvent("adWatched", new Dictionary<string, object>
            {
                {"score", (int)currentScore },
                {"distance", (int)shipBehaviour.GetDistanceTraveled() }
            });
        }

        PlayerPrefs.SetInt("AD_COUNT", Random.Range(3, 6));
    }
}
