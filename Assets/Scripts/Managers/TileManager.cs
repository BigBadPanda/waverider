using UnityEngine;
using System.Collections.Generic;

public class TileManager : MonoBehaviour {

    public int currentLevel;
    public PieceStyle currentStyle;

    [Header("Tile Set Prefabs")]
	public Transform tileSetsRoot;

    private List<Tile>[] Tiles;

    [Header("Cycle parameters")]
    public int screenSize = 45;
    public float resetLimit = 140;
	public float recycleOffset;
	public Vector3 startPosition;

    [Header("Booster parameters")]
    public float booster_minGap = 200;
    public float booster_probability = 0.15f;
    private float booster_lastDistanceSpawned;
	
	private Vector3 nextPosition;
    private float lastEnvironmentChange;
    private int currentSize;
	private Queue<Tile> objectQueue;
    
    private ShipBehaviour player;
    private CameraFollow cam;

    private bool tutorialEnabled;
    private Tile tutorialTile;

    void Awake()
	{
        player = FindObjectOfType<ShipBehaviour>();
        cam = FindObjectOfType<CameraFollow>();

        Tiles = new List<Tile>[tileSetsRoot.childCount];
        objectQueue = new Queue<Tile>(screenSize);
		nextPosition = startPosition;

        for (int i = 0; i < tileSetsRoot.childCount; i++)
        {
            Tiles[i] = new List<Tile>(tileSetsRoot.GetChild(i).GetComponentsInChildren<Tile>(true));
        }
    }

	void Start ()
    {
        if (tutorialEnabled)
            AddTile(tutorialTile);
        else
            AddTile();

        //setting first group of tiles
        while (currentSize < screenSize + (int)(objectQueue.Peek().tileSize * 0.5f))
        {
            AddTile();
        }
	}
	
	void LateUpdate () {
        float playerPos = player.transform.position.z;

		if (objectQueue.Peek().calculatedZ + recycleOffset < playerPos) 
		{
			Recycle();
		}
		
		if (playerPos > resetLimit)
		{
			ResetWorld ();
		}

        float distance = player.GetDistanceTraveled();
        if(currentLevel < 2 && distance > 600)
        {
            currentLevel = 2;
        }
        else if(currentLevel < 1 && distance > 200)
        {
            currentLevel = 1;
        }

        if(distance - lastEnvironmentChange > 600)
        {
            PieceStyle style = (PieceStyle)Random.Range(0, 3);
            EventManager.TriggerEvent(Events.STYLE_CHANGED, style);
            currentStyle = style;
            lastEnvironmentChange = distance;
        }
    }

	//remove last tile and fill with new ones
	private void Recycle () {
		RemoveTile (objectQueue.Dequeue());

		//Recycle till screen is full of tiles
		while (currentSize < screenSize + (int)(objectQueue.Peek().tileSize))
			AddTile ();

	}

    //sets new tile
    private void AddTile(Tile tile = null) {

        while (tile == null)
        {
            int tileIndex = Mathf.Clamp(Random.Range(0, currentLevel + 1), 0, Tiles.Length - 1);
            tile = GetTile(tileIndex);
        }

        Vector3 position = nextPosition;
        tile.transform.position = position;

		position.z += tile.tileSize;
		nextPosition = position; 			
		
		currentSize += tile.tileSize;
        
		objectQueue.Enqueue (tile);
	}

	//get randomly a new tile 
	private Tile GetTile(int tileLevel)
	{
        if (Tiles[tileLevel].Count == 0)
        {
            Debug.LogWarning("GetTile returned null, tile level: " + tileLevel);
            return null;
        }

        Tile result = (Tiles[tileLevel])[Random.Range(0, Tiles[tileLevel].Count)];
        result.SetStyle(currentStyle);
        Tiles[tileLevel].Remove(result);

        if (result != null) {
			result.gameObject.SetActive (true);
            if(player.GetDistanceTraveled() - booster_lastDistanceSpawned > booster_minGap && Random.value < booster_probability)
            {
                booster_lastDistanceSpawned = player.GetDistanceTraveled();
                result.Randomize(true);
            }
            else
            {
                result.Randomize();
            }
		}
		return result;
	}

	//disables given tile and adds to list for pooling
	private void RemoveTile(Tile tile)
	{
		currentSize -= tile.tileSize;

        if(tile.isTutorial)
        {
            Destroy(tile.gameObject);
        }
        else
        {
		    tile.transform.localPosition = Vector3.zero;
            Tiles[tile.tileLevel].Add(tile);

            tile.Remove();
        }
	}

	private void ResetWorld ()
	{
        float zOffset = player.transform.position.z;
        player.ResetPosition();
        cam.Reposition();

		foreach (Tile tile in objectQueue)
        {
			tile.transform.position -= Vector3.forward * zOffset;
		}

		nextPosition.z -= zOffset;
	}
    
    private void IncreasedLevel()
    {
        currentLevel++;
    }
    
    private void DecreaseLevel()
    {
        currentLevel--;
    }

    public void SetTutorial(Tile tutorialTile)
    {
        this.tutorialTile = tutorialTile;
        tutorialEnabled = true;
    }
}