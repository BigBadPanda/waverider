﻿using System.Collections;
using UnityEngine;

public class WindzoneManager : MonoBehaviour
{
    public Transform windParticle;

    public Vector2 windInterval;
    public Vector2 windDuration;

    private int currentWindZone = -3;

    ShipController ship;
    
	void Start ()
    {
        ship = FindObjectOfType<ShipController>();
        StartCoroutine(windProcess());
	}
    
    IEnumerator windProcess()
    {
        yield return new WaitForSeconds(Random.Range(windInterval.x, windInterval.y));
        while (true)
        {
            currentWindZone = Random.Range(-1, 2);

            windParticle.localPosition = Vector3.right * currentWindZone * 4;
            windParticle.gameObject.SetActive(true);
            ship.CheckWind();
            yield return new WaitForSeconds(Random.Range(windDuration.x, windDuration.y));

            windParticle.gameObject.SetActive(false);
            currentWindZone = -3;
            EventManager.TriggerEvent(Events.WINDZONE_CHANGE, false);
            yield return new WaitForSeconds(Random.Range(windInterval.x, windInterval.y));
        }
    }

    public int GetCurrentWindZone()
    {
        return currentWindZone;
    }
}
