using UnityEngine;
using System.Collections.Generic;

public class EnvironmentManager: MonoBehaviour
{
    public bool isMenuScene;
    public PieceStyle currentStyle;

    [Header("Tile Set Prefabs")]
	public Transform tileSetsRoot;

    private List<Tile>[] Tiles;

    [Header("Cycle parameters")]
    public int screenSize = 45;
    public float resetLimit = 140;
	public float recycleOffset;
	public Vector3 startPosition;
	
	private Vector3 nextPosition;
	private int currentSize;
	private Queue<Tile> objectQueue;
    
    private ShipController player;

    void Awake()
	{
        player = FindObjectOfType<ShipController>();


        Tiles = new List<Tile>[tileSetsRoot.childCount];

        for (int i = 0; i < tileSetsRoot.childCount; i++)
        {
            Tiles[i] = new List<Tile>(tileSetsRoot.GetChild(i).GetComponentsInChildren<Tile>(true));
        }

        
    }

	void Start () 
	{
        objectQueue = new Queue<Tile>(screenSize);
		nextPosition = startPosition;

        AddTile();
        //setting first group of tiles
        while (currentSize < screenSize + (int)(objectQueue.Peek().tileSize * 0.5f))
        {
            AddTile();
        }
	}
	
	void LateUpdate ()
    {
        float playerPos = player.transform.position.z;
        if (objectQueue.Peek().calculatedZ + recycleOffset < playerPos) 
		{
			Recycle();
		}
		
		if (playerPos > resetLimit)
		{
			ResetWorld ();
		}

	}

	//remove last tile and fill with new ones
	private void Recycle ()
    {
		RemoveTile (objectQueue.Dequeue());

		//Recycle till screen is full of tiles
		while (currentSize < screenSize + (int)(objectQueue.Peek().tileSize))
			AddTile ();

	}
    
    //sets new tile
    private void AddTile()
    {
		Tile tile = null	;

        while (tile == null)
        {
            tile = GetTile((int)currentStyle);
        }

        Vector3 position = nextPosition;
        tile.transform.position = position;

		position.z += tile.tileSize;
		nextPosition = position; 			
		
		currentSize += tile.tileSize;
        
		objectQueue.Enqueue (tile);
	}

	//get randomly a new tile 
	private Tile GetTile(int tileLevel)
	{
        if (Tiles[tileLevel].Count == 0)
        {
            Debug.LogWarning("GetTile returned null, tile level: " + tileLevel);
            return null;
        }

        Tile result = (Tiles[tileLevel])[Random.Range(0, Tiles[tileLevel].Count)];
        Tiles[tileLevel].Remove(result);

        if (result != null) {
			result.gameObject.SetActive (true);
		}
		return result;
	}

	//disables given tile and adds to list for pooling
	private void RemoveTile(Tile tile)
	{
		tile.transform.localPosition = Vector3.zero;
		currentSize -= tile.tileSize;
        Tiles[tile.tileLevel].Add(tile);

        tile.gameObject.SetActive (false);
	}

	private void ResetWorld ()
	{
        float zOffset = player.transform.position.z;
        if(isMenuScene)
        {
            player.ResetPosition();
        }

		foreach (Tile tile in objectQueue)
        {
			tile.transform.position -= Vector3.forward * zOffset;
		}

		nextPosition.z -= zOffset;
	}

    void StyleChanged(object data)
    {
        currentStyle = (PieceStyle) data;
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.STYLE_CHANGED, StyleChanged);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.STYLE_CHANGED, StyleChanged);
    }
}