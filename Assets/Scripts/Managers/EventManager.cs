using UnityEngine.Events;
using System.Collections.Generic;

public class DataEvent : UnityEvent <object> {}

public enum Events
{
    PLAYER_SCORED,
    PLAYER_DIED,
    WINDZONE_CHANGE,
    MULTIPLIER_RECEIVED,
    STYLE_CHANGED
}

public class EventManager {

	private Dictionary <object, DataEvent> eventDictionary;

	private static EventManager eventManager;

	public static EventManager instance
	{
		get
		{
			if (eventManager == null)
			{
				eventManager = new EventManager();
				eventManager.Init (); 
			}

			return eventManager;
		}
	}

	void Init ()
	{
		if (eventDictionary == null)
		{
			eventDictionary = new Dictionary<object, DataEvent>();
		}
	}

	public static void StartListening (object eventName, UnityAction<object> listener)
	{
		DataEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.AddListener (listener);
		} 
		else
		{
			thisEvent = new DataEvent ();
			thisEvent.AddListener (listener);
			instance.eventDictionary.Add (eventName, thisEvent);
		}
	}

	public static void StopListening (object eventName, UnityAction<object> listener)
	{
		if (eventManager == null) return;
		DataEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.RemoveListener (listener);
		}
	}

	public static void TriggerEvent (object eventName, object data = null)
	{
		DataEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.Invoke (data);
		}
	}
}