using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioType
{
    LANE_SWAP,
    POWER_UP,
    POWER_UP_SPEED
}

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [Header("Audio Clips")]
    public AudioClip windzoneAudio;
    public AudioClip laneswapAudio;
    public AudioClip shipmovementAudio;
    public AudioClip powerUpAudio;
    public AudioClip powerUpSpeedAudio;

    [Header("Audio Source Parameters")]
    public float windVolume = 1;
    public float shipMovementVolume = 1; 
    public float oneShotVolume = 1;

    private AudioSource windSource;
    private AudioSource shipMovementSource;
    private AudioSource oneShotAudioSource;

    public bool isInitilized;

	void Awake ()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        InitilizeAudioSources();
    }

    void InitilizeAudioSources()
    {
        if (PlayerPrefs.GetInt("SFX") != 1)
        {
            return;
        }

        windSource = gameObject.AddComponent<AudioSource>();
        shipMovementSource = gameObject.AddComponent<AudioSource>();
        oneShotAudioSource = gameObject.AddComponent<AudioSource>();

        windSource.volume = windVolume;
        oneShotAudioSource.volume = oneShotVolume;
        shipMovementSource.volume = shipMovementVolume;

        shipMovementSource.clip = shipmovementAudio;
        shipMovementSource.loop = true;
        SetActiveShipMovementAudio(true);

        isInitilized = true;
    }

    public void SetActiveShipMovementAudio(bool enable)
    {
        if (enable)
            shipMovementSource.Play();
        else
            shipMovementSource.Stop();
    }


    public void PlayOneShot(AudioType type)
    {
        if (!isInitilized)
            return;

        switch (type)
        {
            case AudioType.LANE_SWAP:
                oneShotAudioSource.PlayOneShot(laneswapAudio);
                break;
            case AudioType.POWER_UP:
                oneShotAudioSource.PlayOneShot(powerUpAudio);
                break;
            case AudioType.POWER_UP_SPEED:
                oneShotAudioSource.PlayOneShot(powerUpSpeedAudio);
                break;
            default:
                break;
        }
    }

    public void SetActiveWindzoneAudio(bool enable)
    {
        if (!isInitilized)
            return;

        StartCoroutine(windSoundFade(enable));
    }

    IEnumerator windSoundFade(bool enable)
    {
        windSource.clip = windzoneAudio;
        windSource.Play();

        float oneOverDuration = 5f; // 1 / 0.2f
        if (enable)
        {
            for (float i = 0; i <= 0.2f; i += Time.deltaTime)
            {
                windSource.volume = i * oneOverDuration;
                yield return null;
            }
            windSource.volume = windVolume;
        }
        else
        {
            for (float i = 0; i <= 0.2f; i += Time.deltaTime)
            {
                windSource.volume = 1 - (i * oneOverDuration);
                yield return null;
            }
            windSource.volume = 0;
            windSource.Stop();
        }
    }

}
