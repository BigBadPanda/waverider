﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RadialCounter : MonoBehaviour
{
    public float count = 5;

    [Header("Animation objects")]
    public TextMeshProUGUI counterText;
    public Image radialFill;

    private void OnEnable()
    {
        StartCoroutine(Counter());
    }


    IEnumerator Counter()
    {
        float t = count;
        float one_overCount = 1 / count;

        radialFill.fillAmount = 1;
        counterText.text = count.ToString();

        while (t > 0)
        {
            radialFill.fillAmount = t * one_overCount;
            counterText.text = Mathf.CeilToInt(t).ToString();
            t -= Time.deltaTime;
            yield return null;
        }
        radialFill.fillAmount = 0;
        counterText.text = "0";
    }
}
