﻿using UnityEngine;
using TMPro;

public class UIElementController : MonoBehaviour {

    public TextMeshProUGUI textField;

    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void Enter(string text = null)
    {
        if (textField != null)
            textField.text = text;

        anim.SetTrigger("Enter");
    }

    public void Exit()
    {
        anim.SetTrigger("Exit");
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
