using System;

[Serializable]
public class GameData
{
    public string version = "1.0";

    public int[] boosterCount;
    public long lastBonusTime;
    public int boosterSpinCount;

    public GameData(int[] boosterCount)
    {
        this.boosterCount = boosterCount;
    }

    public GameData()
    {
        this.boosterCount = new int[3];
    }
}
/* Version 1.0 
 * ------------
 * 0 Life/ 1 Double Score/ 2 Magnet 
*/

