using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad
{
    private static GameData gameData;

    public static GameData GetGameData()
    {
        if(gameData == null)
        {
            gameData = SaveLoad.Load();
        }

        return gameData;
    }

	public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gameData.gd");
        try
        {
            bf.Serialize(file, gameData);
        }
        catch (System.Exception e)
        {
            Debug.LogWarning(e);
        }
        file.Close();
    }

    public static GameData Load()
    {
        if (File.Exists(Application.persistentDataPath + "/gameData.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gameData.gd", FileMode.Open);
            gameData = (GameData) bf.Deserialize(file);
            file.Close();
            Debug.Log("GameData loaded succesfully");
        }
        else
        {
            Debug.LogWarning("GameData not found");
            gameData = new GameData();
        }
        return gameData;
    }
}
