﻿using UnityEngine;

public class PieceSpawner : MonoBehaviour
{
    public PieceType typeToSpawn;

    private GameObject piece;

   

    public void Spawn(PieceStyle style)
    {
        piece = PiecePool.Instance.GetPiece(typeToSpawn, style);
        if (piece == null)
            return;
        piece.transform.SetParent(transform);
        piece.transform.localPosition = Vector3.zero;
    }

    public void Despawn()
    {
        if (piece == null)
            return;
        
        PiecePool.Instance.RemovePiece(piece);
        piece = null;
    }
    
    private void OnDrawGizmos()
    {
        name = typeToSpawn.ToString() + "_Spawner";
        Gizmos.color = Color.black;
        switch (typeToSpawn)
        {
            case PieceType.PIECE_1:
                Gizmos.DrawCube(transform.position, new Vector3(1,1,1)); 
                break;
            case PieceType.PIECE_2:
                Gizmos.DrawCube(transform.position, new Vector3(2, 1, 2));
                break;
            case PieceType.PIECE_4:
                Gizmos.DrawCube(transform.position, new Vector3(2, 1, 4));
                break;
            case PieceType.PIECE_6:
                Gizmos.DrawCube(transform.position, new Vector3(2, 1, 6));
                break;
            case PieceType.MOVING_PIECE:
                Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
                break;
            default:
                break;
        }
        Gizmos.color = Color.white;
    }
}
