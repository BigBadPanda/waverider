﻿using UnityEngine;

[RequireComponent(typeof(TrailRenderer))]
public class TrailRendererReset : MonoBehaviour {

    TrailRenderer tr;

	void Awake ()
    {
        tr = GetComponent<TrailRenderer>();	
	}
	
    public void ResetTrailRenderer()
    {
        tr.Clear();
    }

    public void EnableTrail()
    {
        tr.Clear();
        tr.gameObject.SetActive(true);
    }

    public void DisableTrail()
    {
        tr.gameObject.SetActive(false);
    }
}
