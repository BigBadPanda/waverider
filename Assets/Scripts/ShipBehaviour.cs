using System.Collections;
using UnityEngine;

public class ShipBehaviour : MonoBehaviour
{
    [Header("Baloon Booster")]
    [SerializeField] private float baloon_boosterSpeed;
    [SerializeField] private float baloon_boosterRampTime;
    [SerializeField] private float baloon_boosterDuration;

    [Header("Score Multiplier Booster")]
    [SerializeField] private float scoreMultiplierDuration = 10;

    [Header("Second Chance")]
    [SerializeField] private float secondChanceDuration = 5;

    [Header("Effects")]
    [SerializeField] private ImmunityEffect immunity_Effect;
    [SerializeField] private GameObject speed_Effect;

    ShipController controller;
    private float distanceChange, distanceTraveled;

    private bool isGameOver;
    private bool isImmune;

    private void Awake()
    {
        controller = GetComponent<ShipController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Obstacle":
                Die(other.name);
                break;
            case "Booster":
                print(other.name);
                ProcessBooster(other.GetComponent<Booster>());
                break;
            default:
                break;
        }

    }

    private void Update()
    {
        if (isGameOver)
            return;

        CheckDistanceChange();
    }

    void Die(string hitTarget)
    {
        if (isImmune)
            return;

        controller.StopMovement();
        isGameOver = true;
        speed_Effect.SetActive(false);
        EventManager.TriggerEvent(Events.PLAYER_DIED);
    }

    void CheckDistanceChange()
    {
        float speed = controller.speed;
        float dDistance = speed * Time.deltaTime;
        distanceChange += dDistance;
        distanceTraveled += dDistance;
        if (distanceChange > 5)
        {
            distanceChange -= 5;

            EventManager.TriggerEvent(Events.PLAYER_SCORED);
        }
    }

    void ProcessBooster(Booster b)
    {
        if (b == null)
            return;

        AudioManager.Instance.PlayOneShot(AudioType.POWER_UP);

        if(b.tutorialPiece)
        {
            Destroy(b.gameObject);
        }
        else
        {
            PiecePool.Instance.RemovePiece(b.gameObject);
        }

        switch (b.type)
        {
            case BoosterType.BALLOON:
                StartCoroutine(BalloonBooster());
                break;
            case BoosterType.DOUBLE_SCORE:
                StartCoroutine(ScoreBooster(2));
                break;
            case BoosterType.TRIPLE_SCORE:
                StartCoroutine(ScoreBooster(3));
                break;
            default:
                break;
        }
    }

    public float GetDistanceTraveled()
    {
        return distanceTraveled;
    }

    public void ResetPosition()
    {
        controller.ResetPosition();
    }

    IEnumerator ScoreBooster(int multiplier)
    {
        EventManager.TriggerEvent(Events.MULTIPLIER_RECEIVED, multiplier);
        yield return new WaitForSeconds(scoreMultiplierDuration);
        EventManager.TriggerEvent(Events.MULTIPLIER_RECEIVED, 0);
    }

    IEnumerator BalloonBooster()
    {
        SetImmunity(true, baloon_boosterDuration + baloon_boosterRampTime);
        speed_Effect.SetActive(true);
        AudioManager.Instance.PlayOneShot(AudioType.POWER_UP_SPEED);
        //speed_Effect.GetComponentInChildren<ParticleSystem>().Clear();
        float initialSpeed = controller.speed;
        float time = baloon_boosterRampTime;
        float deltaSpeed = (baloon_boosterSpeed - initialSpeed) / baloon_boosterRampTime;

        while(time > 0)
        {
            controller.speed += deltaSpeed * Time.deltaTime;
            time -= Time.deltaTime;
        }
        controller.speed = baloon_boosterSpeed;
        
        yield return new WaitForSeconds(baloon_boosterDuration);
        controller.speed = initialSpeed;
        SetImmunity(false);
        speed_Effect.SetActive(false);
    }

    IEnumerator SecondChanceImmunity()
    {
        SetImmunity(true, secondChanceDuration);
        yield return new WaitForSeconds(secondChanceDuration);
        SetImmunity(false);
    }

    void SetImmunity(bool val, float duration = 0)
    {
        isImmune = val;
        if(val)
        {
            immunity_Effect.SetDuration(duration);
        }
        else
        {
            immunity_Effect.gameObject.SetActive(false);
        }
    }

    public void SecondChance()
    {
        controller.StartMovement();
        StartCoroutine(SecondChanceImmunity());
        isGameOver = false;
    }

    public void TriggerAnimation(string animation)
    {
        controller.TriggerAnimation(animation);
    }

    void WindzoneChange(object data)
    {
        if (isGameOver)
            return;

        speed_Effect.SetActive((bool)data);
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.WINDZONE_CHANGE, WindzoneChange);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.WINDZONE_CHANGE, WindzoneChange);
    }
}
