﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Renderer))]
class AnimateTiledTexture : MonoBehaviour
{
    public int columns = 2;
    public int rows = 2;
    public float framesPerSecond = 10f;
    public bool reverse = false;

    //the current frame to display
    private int index = 0;
    private Material sm;

    void Awake()
    {
        sm = GetComponent<Renderer>().sharedMaterial;

        //set the tile size of the texture (in UV units), based on the rows and columns
        Vector2 size = new Vector2(1f / columns, 1f / rows);
        sm.SetTextureScale("_MainTex", size);
    }

    private IEnumerator updateTiling()
    {
        while (true)
        {
            //move to the next index
            index = (reverse) ? index - 1 : index + 1;

            if (index >= rows * columns)
                index = 0;
            else if (index < 0)
                index = (rows * columns) - 1;

            //split into x and y indexes
            Vector2 offset = new Vector2((float)index / columns - (index / columns), //x index
                                          (index / columns) / (float)rows);          //y index

            sm.SetTextureOffset("_MainTex", offset);

            yield return new WaitForSeconds(1f / framesPerSecond);
        }

    }

    private void OnEnable()
    {
        StartCoroutine(updateTiling());
    }

}