using UnityEngine;
using System.Collections.Generic;

public class Tile : MonoBehaviour {

    public bool isEnvironment;
    public bool isTutorial;
    public int tileSize;
    public int tileLevel;
    private int tileHalfWidth = 6;
    private PieceStyle currentStyle;

    private List<Transform> obstacles_R;
    private List<Transform> obstacles_L;
    private List<BoatSpawner> boatSpawners;
    private List<PieceSpawner> pieceSpawners;
    private List<BoosterPosition> boosterPositions;

    private GameObject booster;

    void Awake()
	{
        if (isEnvironment)
            return;

        boatSpawners = new List<BoatSpawner>(GetComponentsInChildren<BoatSpawner>());
        pieceSpawners = new List<PieceSpawner>(GetComponentsInChildren<PieceSpawner>());
        boosterPositions = new List<BoosterPosition>(GetComponentsInChildren<BoosterPosition>());
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position - (transform.right * tileHalfWidth), transform.position + (transform.right * tileHalfWidth));
        Gizmos.DrawLine(transform.position - (transform.right * tileHalfWidth) + (transform.forward * tileSize), transform.position + (transform.right * tileHalfWidth) + (transform.forward * tileSize));

        if(isEnvironment)
        {
            Gizmos.DrawLine(transform.position - (transform.right * tileHalfWidth), transform.position - (transform.right * tileHalfWidth) + (transform.forward * tileSize));
            Gizmos.DrawLine(transform.position + (transform.right * tileHalfWidth), transform.position + (transform.right * tileHalfWidth) + (transform.forward * tileSize));
        }
    }

    public float calculatedZ
    {
		get { return transform.position.z + tileSize; }
	}

    public void Randomize(bool boosterEnabled = false)
    {
        int randomizer = (Random.value > 0.5f) ? 1 : -1;
        
        foreach (BoatSpawner spawner in boatSpawners)
        {
            Vector3 pos = spawner.transform.localPosition;
            pos.x = pos.x * randomizer;
            spawner.transform.localPosition = pos;
        }

        foreach (BoosterPosition position in boosterPositions)
        {
            Vector3 pos = position.transform.localPosition;
            pos.x = pos.x * randomizer;
            position.transform.localPosition = pos;
        }

        foreach (PieceSpawner spawner in pieceSpawners)
        {
            spawner.Spawn(currentStyle);
            Vector3 pos = spawner.transform.localPosition;
            pos.x = pos.x * randomizer;
            spawner.transform.localPosition = pos;
        }

        if(boosterEnabled)
            SpawnBooster();
    }

    public void Activate()
    {
        foreach (BoatSpawner spawner in boatSpawners)
        {
            spawner.SpawnBoat(currentStyle);
        }
    }
    
    public void SetStyle(PieceStyle style)
    {
        currentStyle = style;
    }

    public bool SpawnBooster()
    {
        if (boosterPositions.Count == 0)
            return false;

        int idx = Random.Range(0, boosterPositions.Count);
        Vector3 pos = boosterPositions[idx].transform.localPosition;
        int length = boosterPositions[idx].length;
        pos.z -= length * 0.5f;
        pos.z += Random.Range(0, length + 1);

        booster = PiecePool.Instance.GetBooster();
        if(booster == null)
        {
            return false;
        }
        else
        {
            booster.transform.SetParent(transform);
            booster.transform.localPosition = pos;
            return true;
        }
    }

    public void Remove()
    {
        foreach (PieceSpawner spawner in pieceSpawners)
        {
            spawner.Despawn();
        }

        foreach (BoatSpawner spawner in boatSpawners)
        {
            spawner.DespawnBoat();
        }

        if (booster != null)
        {
            if(booster.activeSelf)
                PiecePool.Instance.RemovePiece(booster);

            booster = null;
        }

        gameObject.SetActive(false);
    }
}