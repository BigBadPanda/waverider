﻿using UnityEngine;

public class TutorialZone : MonoBehaviour
{
    public int tutorialIdx;

    TutorialManager tm;

    private void Awake()
    {
        tm = FindObjectOfType<TutorialManager>();
    }

    private void OnTriggerEnter(Collider other)
    { 
        if(other.CompareTag("Player"))
            tm.EnterTutorialZone(tutorialIdx);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            tm.ExitTutorialZone(tutorialIdx);
    }
}
