﻿using System.Collections;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    [Header("Movement Parameters")]
    public float speed = 10;
    [SerializeField] private float laneSize;
    [SerializeField] private float laneSwapDuration;
    [SerializeField] private float swipeTreshold = 1;

    private float laneSwapSpeed;
    private int swapDirection = 0;
    private bool swapAvailable = true;

    private int currentLane = 0;
    private int targetLane;

    private bool gameStopped = true;
    private Animator anim;
    private WindzoneManager wm;
    private TrailRendererReset trr;
    
    void Start ()
    {
        wm = FindObjectOfType<WindzoneManager>();
        trr = GetComponentInChildren<TrailRendererReset>();
        anim = GetComponent<Animator>();
        laneSwapSpeed = laneSize / laneSwapDuration;
        targetLane = currentLane;

        StartMovement();
    }
	
	void Update ()
    {
        if (gameStopped)
            return;

        GetInput();
        Move();
	}

    void GetInput()
    {
        swapDirection = 0;
        if (Input.touchCount > 0 )
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Moved)
            {
                swapDirection = (t.deltaPosition.x > swipeTreshold) ? 1 : ((t.deltaPosition.x < -swipeTreshold) ? -1: 0);
            }
            else if(t.phase == TouchPhase.Ended)
            {
                swapAvailable = true;
            }
        }
#if UNITY_EDITOR
        swapDirection = (int)Input.GetAxisRaw("Horizontal");

        if(swapDirection == 0)
        {
            swapAvailable = true;
        }
#endif
    }

    void Move()
    {
        transform.position += Vector3.forward * speed * Time.deltaTime;

        if (!swapAvailable || swapDirection == 0)
            return;

        targetLane = Mathf.Clamp(targetLane + swapDirection, -1, 1);

        swapAvailable = false;
    }

    IEnumerator movementLoop()
    {
        while(true)
        {
            if(currentLane != targetLane)
            {
                yield return movementAnimation();
            }
            else
            {
                yield return null;
            }
        }
    }

    IEnumerator movementAnimation()
    {
        int dir = targetLane - currentLane;

        AudioManager.Instance.PlayOneShot(AudioType.LANE_SWAP);
        if (dir > 0)
        {
            anim.SetTrigger("GoRight");
        }
        else if(dir < 0)
        {
            anim.SetTrigger("GoLeft");
        }

        float t = 0;
        while (t <= laneSwapDuration)
        {

            float dT = Time.deltaTime;
            if(dT > laneSwapDuration - t)
            {
                transform.position += dir * Vector3.right * laneSwapSpeed * (laneSwapDuration - t);
            }
            else
            {
                transform.position += dir * Vector3.right * laneSwapSpeed * dT;
            }
           
            t += dT;
            yield return null;
        }

        currentLane += dir;
        CheckWind();

        transform.position = new Vector3(currentLane * laneSize, 0, transform.position.z);
    }

    public void StopMovement()
    {
        gameStopped = true;
        StopAllCoroutines();
    }
    public void StartMovement()
    {
        if (!gameStopped)
            return;

        gameStopped = false;
        StartCoroutine(movementLoop());
    }

    public void ResetPosition()
    {
        trr.ResetTrailRenderer();
        transform.position = new Vector3(transform.position.x, 0, 0);
    }

    public void CheckWind()
    {
        if (currentLane == wm.GetCurrentWindZone())
            EventManager.TriggerEvent(Events.WINDZONE_CHANGE, true);
        else
            EventManager.TriggerEvent(Events.WINDZONE_CHANGE, false);
    }

    public void TriggerAnimation(string animationName)
    {
        switch (animationName)
        {
            case "Hit":
                anim.SetTrigger("Hit");
                break;
            case "Death":
                anim.SetTrigger("Death");
                break;
            default:
                break;
        }
    }
    
}
