﻿using UnityEngine;

public class BoosterPosition : MonoBehaviour
{
    public BoosterType type;
    public int length = 4;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, new Vector3(1, 1, length));
        Gizmos.color = Color.white;
    }

}
