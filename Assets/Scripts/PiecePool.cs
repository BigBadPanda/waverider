using System.Collections.Generic;
using UnityEngine;
using QuickPool;

public enum PieceType
{
    PIECE_1,
    PIECE_2,
    PIECE_4,
    PIECE_6,
    MOVING_PIECE
}

public enum PieceStyle
{
    Corals,
    OpenSea,
    City
}

[System.Serializable]
public struct PieceStruct
{
    public List<GameObject> prefabs;
    public PieceStyle style;
}

public class PiecePool : MonoBehaviour {

    public static PiecePool Instance;

    [Header("Static Pieces")]
    public List<PieceStruct> pieceObjects_1;
    public List<PieceStruct> pieceObjects_2;
    public List<PieceStruct> pieceObjects_4;
    public List<PieceStruct> pieceObjects_6;
    public List<PieceStruct> pieceObjects_Moving;

    [Header("Dynamic Pieces")]
    public List<PieceStruct> movingPieceObjects_4;
    public List<PieceStruct> movingPieceObjects_6;

    [Header("Boosters")]
    public List<GameObject> boosterObjects;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        
    }

    public GameObject GetPiece(PieceType type, PieceStyle style = 0)
    {
        GameObject p;
        switch (type)
        {
            case PieceType.PIECE_1:
                List<GameObject> pieces = pieceObjects_1.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                int idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                //p.pieceID = idx;
                break;
            case PieceType.PIECE_2:
                pieces = pieceObjects_2.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                //p.pieceID = idx;
                break;
            case PieceType.PIECE_4:
                pieces = pieceObjects_4.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                //p.pieceID = idx;
                break;
            case PieceType.PIECE_6:
                pieces = pieceObjects_6.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                // p.pieceID = idx;
                break;
            case PieceType.MOVING_PIECE:
                pieces = pieceObjects_Moving.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                // p.pieceID = idx;
                break;
            default:
                p = null;
                break;
        }

        return p;
    }

    public void RemovePiece(GameObject p)
    {
        PoolsManager.Despawn(p);
    }

    public GameObject GetMovingPiece(PieceType type, PieceStyle style = 0)
    {
        GameObject p;
        switch (type)
        {
            case PieceType.PIECE_4:
                List<GameObject> pieces = movingPieceObjects_4.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                int idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                //p.pieceID = idx;
                break;
            case PieceType.PIECE_6:
                pieces = movingPieceObjects_6.Find(x => x.style == style).prefabs;
                if (pieces == null || pieces.Count == 0)
                    return null;
                idx = Random.Range(0, pieces.Count);
                p = PoolsManager.Spawn(pieces[idx].name, Vector3.zero, Quaternion.identity);
                //p.pieceID = idx;
                break;
            default:
                p = null;
                break;
        }

        return p;
    }

    public GameObject GetBooster()
    {
        if (boosterObjects.Count == 0)
            return null;

        int idx = Random.Range(0, boosterObjects.Count);
        return PoolsManager.Spawn(boosterObjects[idx].name, Vector3.zero, Quaternion.identity);
    }

}
