﻿using UnityEngine;

public enum BoosterType
{
    BALLOON,
    DOUBLE_SCORE,
    TRIPLE_SCORE
}

public class Booster : MonoBehaviour
{
    public BoosterType type;
    public bool tutorialPiece;
}
