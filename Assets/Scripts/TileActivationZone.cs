﻿using UnityEngine;

public class TileActivationZone : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        Tile t = other.GetComponent<Tile>();
        if (t != null)
        {
            t.Activate();
        }
    }
}
