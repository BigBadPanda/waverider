using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScreenshotCapture : MonoBehaviour
{

    public string path;
    public KeyCode captureKey = KeyCode.C;

    private int screenShotCounter = 0;

    void LateUpdate ()
    {
	    if(Input.GetKeyUp(captureKey))
        {
            string pathStr = string.Format("{0}/Screenshot_{1}.png", path, screenShotCounter);
            while(File.Exists(pathStr))
            {
                screenShotCounter++;
                pathStr = string.Format("{0}/Screenshot_{1}.png", path, screenShotCounter);
            }
            ScreenCapture.CaptureScreenshot(pathStr);
            screenShotCounter++;
        }
	}
}
