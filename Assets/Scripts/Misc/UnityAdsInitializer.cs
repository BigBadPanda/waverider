using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsInitializer : MonoBehaviour
{
    [SerializeField]
    private string
        androidGameId = "1065989",
        iosGameId = "1065990";

    [SerializeField]
    private bool testMode = false;

    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        string gameId = null;
#if UNITY_ANDROID
        gameId = androidGameId;
#elif UNITY_IOS
        gameId = iosGameId;
#endif

        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            Advertisement.Initialize(gameId, testMode);
        }
    }
}