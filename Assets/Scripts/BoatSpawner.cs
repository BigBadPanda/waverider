using UnityEngine;

public class BoatSpawner : MonoBehaviour {

    public PieceType pieceToSpawn;
    public float boatSpeed = 3;
    public float debugTime = 0;
    Transform boat;
	
	// Update is called once per frame
	void Update ()
    {
        if (boat == null)
            return;

        boat.position += Vector3.back  * boatSpeed * Time.deltaTime;
	}

    public void SpawnBoat(PieceStyle style)
    {
        GameObject piece = PiecePool.Instance.GetMovingPiece(pieceToSpawn, style);
        if (piece == null)
            return;

        boat = piece.transform;
        boat.SetParent(transform);
        boat.localPosition = Vector3.zero;
        boat.GetComponentInChildren<TrailRendererReset>(true).EnableTrail();
    }

    public void DespawnBoat()
    {
        if (boat == null)
            return;

        boat.GetComponentInChildren<TrailRendererReset>().DisableTrail();
        PiecePool.Instance.RemovePiece(boat.gameObject);
        boat = null;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(transform.position, new Vector3(2, 1, 1));
        Gizmos.DrawLine(transform.position, transform.position + Vector3.back * boatSpeed * debugTime);
        if(pieceToSpawn == PieceType.PIECE_6)
        {
            Gizmos.DrawCube(transform.position + Vector3.back * boatSpeed * debugTime, new Vector3(2,1,6));
        }
        else if (pieceToSpawn == PieceType.PIECE_4)
        {
            Gizmos.DrawCube(transform.position + Vector3.back * boatSpeed * debugTime, new Vector3(2, 1, 4));
        }
        Gizmos.color = Color.white;
    }

}
