﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public float followDist = 10;
		
	void LateUpdate ()
    {
        Reposition();
    }

    public void Reposition()
    {
        if (target == null) return;

        Vector3 newPos = transform.position;
        newPos.z = target.position.z;

        newPos -= Vector3.forward * followDist;

        transform.position = newPos;
    }
}
